<?php
class Twitter extends Tools_Plugins_Abstract{

    const CACHE_LIFETIME = 3600;

	public function  __construct($options, $seotoasterData) {
		parent::__construct($options, $seotoasterData);
		$this->_seotoasterData = $seotoasterData;
		
		$this->_view->setScriptPath(dirname(__FILE__) . '/system/views/');
	}

    public function _init(){
        $this->_http           = new Zend_Http_Client();
        $this->_twitterAccount = Models_Mapper_Twitter::getInstance()->getConf();
        $this->_cacheHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('cache');
    }
    public function run($requestedParams = array()) {
		$dispatchersResult      = parent::run($requestedParams);
        if(isset($this->_options[0]) && $this->_options[0] == 'followers'){
            $this->_http->setUri('http://api.twitter.com/1/statuses/followers/'.$this->_twitterAccount.'.json');
            $response = (json_decode($this->_http->request()->getBody(), true));
            shuffle($response);
            isset($this->_options[1]) ? $this->_view->imageSize = $this->_options[1] : $this->_view->imageSize = 50;
            isset($this->_options[2]) ? $this->_view->imagesNumber = $this->_options[2] : $this->_view->imagesNumber = 50;
            $this->_sessionHelper->twitterData = array_slice(($response), 0, $this->_view->imagesNumber);
            $this->_view->response = $this->_sessionHelper->twitterData;
            return $this->_view->render('twitter.phtml');
        }
        if(isset($this->_options[0]) && $this->_options[0] == 'tweetts'){
            isset($this->_options[1]) ? $tweetsCount = $this->_options[1] : $tweetsCount = 20;
            $cachedRequestStats = $this->_cacheHelper->load($this->_seotoasterData['url'], 'twitter_plugin_stats');
            if($cachedRequestStats){
                $this->_view->stats  = $cachedRequestStats;
            } else {
                $this->_http->setUri('https://api.twitter.com/1/users/show.json?screen_name='.$this->_twitterAccount);
                $this->_view->stats = json_decode($this->_http->request()->getBody(), true);
                $this->_cacheHelper->save($this->_seotoasterData['url'], $this->_view->stats, 'twitter_plugin_stats', '', self::CACHE_LIFETIME);
            }
            $cachedRequestTweets = $this->_cacheHelper->load($this->_seotoasterData['url'], 'twitter_plugin_tweets');
            if($cachedRequestTweets){
                $this->_view->tweets  = $cachedRequestTweets;
            } else {
                $this->_http->setUri('https://api.twitter.com/1/statuses/user_timeline.json?include_rts=true&screen_name='.$this->_twitterAccount.'&count='.$tweetsCount);
                $this->_view->tweets = json_decode($this->_http->request()->getBody(), true);
                $this->_cacheHelper->save($this->_seotoasterData['url'], $this->_view->tweets, 'twitter_plugin_tweets', '', self::CACHE_LIFETIME);
            }
            return $this->_view->render('twits.phtml');
        }
        if(isset($this->_options[0]) && $this->_options[0] == 'stats'){
            $this->_http->setUri('https://api.twitter.com/1/users/show.json?screen_name='.$this->_twitterAccount);
            $this->_view->stats = json_decode($this->_http->request()->getBody(), true);
            return $this->_view->render('stats.phtml');
        }
    }

    public function configAction(){
        if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS)){
            $this->_view->userName   = Models_Mapper_Twitter::getInstance()->getConf();
            $this->_view->websiteUrl = $this->_seotoasterData['websiteUrl'];
            echo $this->_view->render('config.phtml');
        }
    }

    public function saveConfigAction(){
        if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS)){
            if($this->_request->isPost()){
                Models_Mapper_Twitter::getInstance()->setConf(filter_var($this->_requestedParams['user_name'], FILTER_SANITIZE_STRING ));
            }
        }
        echo '<script>top.location.reload()</script>';
    }
}