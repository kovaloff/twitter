$(document).ready(function(){
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/igm;
    var twitterNames = /@([A-Za-z0-9_]+)/igm
    var hashTags = /#([A-Za-z0-9_]+)/igm
    $.each($('.twitter_body'), function(index,value){
        $(this).html(value.textContent.replace(exp,"<a target=\"_blank\" href='$1'>$1</a>").replace(twitterNames, "<a target=\"_blank\" href='http://twitter.com/$1'>@$1</a>").replace(hashTags, "<a target=\"_blank\" href='http://search.twitter.com/search?q=$1'>#$1</a>"))
    })
})