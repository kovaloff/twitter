<?php

class Models_Mapper_Twitter extends Application_Model_Mappers_Abstract{
	
	protected $_dbTable = 'Models_DbTable_Twitter';

	protected $_model   = 'Models_Model_Twitter';
	
	public function save($_model){}
	
    public function getConf(){
		if($this->getDbTable()->fetchAll()->current())
		{
			$accountData =  $this->getDbTable()->fetchAll()->current()->toArray();
            return $accountData['name'];
		}
		else
		{
			return false;
		};
    }
    public function setConf($user){
           $where = ('id = 1');
            $data = array(
                "name"		  => $user
            );
		 return $this->getDbTable()->update($data, $where);
	}
}